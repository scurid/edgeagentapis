# README #

Scurid Edge Agent gRPC proto file

This project now also houses .proto files for Microscurid projects.

## EdgeAgent vs MicroScurid Projects
* EdgeAgent is focused for non-microcontroller platforms, while MicroScurid is focused purely on microcontrollers
* EdgeAgent is designed for some autonomous features, so it needs to run as a service, while MicroScurid is designed like an SDK with little to no automation in
* EdgeAgent proto contains both the RPC and message definitions, while MicroScurid protos only contain message definitions, this is because MicroScurid does not support gRPC based communication rather message structure is used for exchanging data over simple TCP (Client/Server) architecture

## Helpful reads
* [Proto v2 vs V3](https://cloud.google.com/apis/design/proto3)
* [Nanopb supports Proto v3](https://github.com/nanopb/nanopb/issues/159)

## Using optional in proto3
If you try to run protoc on a file with proto3 optional fields, you will get an error because the feature is still experimental, use flag `--experimental_allow_proto3_optional`
[Read more](https://github.com/protocolbuffers/protobuf/blob/main/docs/implementing_proto3_presence.md#satisfying-the-experimental-check)