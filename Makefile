protos:
	protoc -I pkg/grpc/proto/ pkg/grpc/proto/edgeagent.proto --experimental_allow_proto3_optional --go_out=../../../ --go-grpc_out=../../../ --descriptor_set_out=edgeagent.pb --grpc-gateway_out=:../../../  --include_imports --include_source_info --grpc-gateway_opt logtostderr=true --grpc-gateway_opt generate_unbound_methods=true

	protoc -I pkg/grpc/proto/ pkg/grpc/proto/microscurid.proto --experimental_allow_proto3_optional --go_out=../../../ --go-grpc_out=../../../ --descriptor_set_out=microscurid.pb --grpc-gateway_out=:../../../  --include_imports --include_source_info --grpc-gateway_opt logtostderr=true --grpc-gateway_opt generate_unbound_methods=true

	protoc -I pkg/grpc/proto/ pkg/grpc/proto/deviceContext.proto --experimental_allow_proto3_optional --go_out=../../../ --go-grpc_out=../../../  --grpc-gateway_out=:../../../  --include_imports --include_source_info --grpc-gateway_opt logtostderr=true --grpc-gateway_opt generate_unbound_methods=true

	protoc -I pkg/grpc/proto pkg/grpc/proto/errors/edgeagent.proto --experimental_allow_proto3_optional --go_out=../../../ --go-grpc_out=../../../

	protoc -I pkg/grpc/proto pkg/grpc/proto/errors/microscuridc.proto --experimental_allow_proto3_optional --go_out=../../../ --go-grpc_out=../../../

docgen:
	echo "Generating EdgeAgent API documentation"
	docker run --rm -v /Users/sushantpandey/go/src/bitbucket.org/scurid/edgeagentapis/pkg/grpc/docs/edgentapi:/out -v /Users/sushantpandey/go/src/bitbucket.org/scurid/edgeagentapis/pkg/grpc/proto:/protos pseudomuto/protoc-gen-doc --doc_opt=markdown,edgeagent.md edgeagent.proto
	echo "Generating MicroScurid-C API documentation"
	docker run --rm -v /Users/sushantpandey/go/src/bitbucket.org/scurid/edgeagentapis/pkg/grpc/docs/microscurid:/out -v /Users/sushantpandey/go/src/bitbucket.org/scurid/edgeagentapis/pkg/grpc/proto:/protos pseudomuto/protoc-gen-doc --doc_opt=markdown,microscurid.md microscurid.proto