
�p
edgeagent.protoedgeagentapis.v1"
GetDeviceUsersReq"T
GetDeviceUsersRes?
deviceUsers ( 2.edgeagentapis.v1.DeviceUsersR deviceUsers"A
DeviceUsers
email (  Remail
sessionID (  R  sessionID"
Empty"T
BiometricAuthReq
id (  Rid
email (  Remail
agentDID (  RagentDID"*
BiometricAuthRes
result (Rresult"�
!SendDeviceDataWithCustomFieldsReq
agentDID (  RagentDID
data (  RdataA

influxData ( 2.edgeagentapis.v1.InfluxDataH R
influxData�B
_influxData";
!SendDeviceDataWithCustomFieldsRes
result (Rresult"+
GetOnboardingPackageReq
did (  Rdid"?
GetOnboardingPackageRes$

onboardingPkg (  R
onboardingPkg")
GetTokenReq
username (  Rusername"#
GetTokenRes
token (  Rtoken"^
VerifySignatureReq
signature (  R  signature
payload (  Rpayload
did (  Rdid".
VerifySignatureRes
isValid (RisValid"A
SignWithIdentityReq
payload (  Rpayload
did (  Rdid"3
SignWithIdentityRes
signature (  R  signature"L
SignBytePayloadWithIdentityReq
payload ( Rpayload
did (  Rdid">
SignBytePayloadWithIdentityRes
signature (  R  signature"l
VerifyBytePayloadWithIdentityReq
signature (  R  signature
payload ( Rpayload
did (  Rdid"<
VerifyBytePayloadWithIdentityRes
isValid (RisValid"
CreateDeviceIdentityReq"+
CreateDeviceIdentityRes
did (  Rdid"+
DeleteDeviceIdentityReq
did (  Rdid"1
DeleteDeviceIdentityRes
result (Rresult"(
GetDeviceIdentityReq
did (  Rdid"(
GetDeviceIdentityRes
did (  Rdid"B
GetScuridEdgeAgentVersionRes"
agentVersion (  R agentVersion"i
RegisterDeviceIdentityReq
did (	Rdid
unixTime (RunixTime

deviceName (	R
deviceName"3
RegisterDeviceIdentityRes
result (Rresult"B
DownloadFilesReq
identity (  Ridentity
path (  Rpath"*
DownloadFilesRes
result (Rresult"O
SendDeviceDataReq
identity (  Ridentity

deviceData (  R
deviceData"+
SendDeviceDataRes
result (Rresult"�

InfluxData:
tags ( 2&.edgeagentapis.v1.InfluxData.TagsEntryRtags@
fields ( 2(.edgeagentapis.v1.InfluxData.FieldsEntryRfields7
TagsEntry
key (  Rkey
value (  Rvalue: 8R
FieldsEntry
key (  Rkey-
value ( 2.edgeagentapis.v1.ValueRvalue: 8"�
Value
int_value (H RintValue#
double_value (H R doubleValue#
string_value (  H R stringValue

bool_value (H R  boolValueB
value"-
+CreateDeviceIdentityWithAdditionalCryptoReq"�
+CreateDeviceIdentityWithAdditionalCryptoRes
did (  RdidJ
ECDSA512_pub_key ( 2 .edgeagentapis.v1.ECDSAPubKey512RECDSA512PubKey:

AES128_key ( 2.edgeagentapis.v1.AES128KeyR  AES128Key"8
ECDSAPubKey512
pubX ( RpubX
pubY ( RpubY"K
AES128Key
keyBits ( RkeyBits$

keyBitsString (  R
keyBitsString2�
ScuridEdgeAgentAPIn
CreateDeviceIdentity).edgeagentapis.v1.CreateDeviceIdentityReq).edgeagentapis.v1.CreateDeviceIdentityRes" i
DeleteDeviceDID).edgeagentapis.v1.DeleteDeviceIdentityReq).edgeagentapis.v1.DeleteDeviceIdentityRes" e
GetDeviceIdentity&.edgeagentapis.v1.GetDeviceIdentityReq&.edgeagentapis.v1.GetDeviceIdentityRes" f
GetScuridEdgeAgentVersion.edgeagentapis.v1.Empty..edgeagentapis.v1.GetScuridEdgeAgentVersionRes" b
SignWithIdentity%.edgeagentapis.v1.SignWithIdentityReq%.edgeagentapis.v1.SignWithIdentityRes" _
VerifySignature$.edgeagentapis.v1.VerifySignatureReq$.edgeagentapis.v1.VerifySignatureRes" �
SignBytePayloadWithIdentity0.edgeagentapis.v1.SignBytePayloadWithIdentityReq0.edgeagentapis.v1.SignBytePayloadWithIdentityRes" �
VerifyBytePayloadWithIdentity2.edgeagentapis.v1.VerifyBytePayloadWithIdentityReq2.edgeagentapis.v1.VerifyBytePayloadWithIdentityRes" t
RegisterDeviceIdentity+.edgeagentapis.v1.RegisterDeviceIdentityReq+.edgeagentapis.v1.RegisterDeviceIdentityRes" J
GetToken.edgeagentapis.v1.GetTokenReq.edgeagentapis.v1.GetTokenRes" Y

DownloadFiles".edgeagentapis.v1.DownloadFilesReq".edgeagentapis.v1.DownloadFilesRes" \
SendDeviceData#.edgeagentapis.v1.SendDeviceDataReq#.edgeagentapis.v1.SendDeviceDataRes" �
SendDeviceDataWithCustomFields3.edgeagentapis.v1.SendDeviceDataWithCustomFieldsReq3.edgeagentapis.v1.SendDeviceDataWithCustomFieldsRes" Y

BiometricAuth".edgeagentapis.v1.BiometricAuthReq".edgeagentapis.v1.BiometricAuthRes" \
GetDeviceUsers#.edgeagentapis.v1.GetDeviceUsersReq#.edgeagentapis.v1.GetDeviceUsersRes" �
(CreateDeviceIdentityWithAdditionalCrypto=.edgeagentapis.v1.CreateDeviceIdentityWithAdditionalCryptoReq=.edgeagentapis.v1.CreateDeviceIdentityWithAdditionalCryptoRes" B:Z8bitbucket.org/scurid/edgeagentapis/pkg/grpc/edgeagent/v1J�K
  �

  

     

     O

  O
�
    N2t provides set of services that will be exposed as APIs for -
Edge clients looking to interact with ScuridEdgeAgent



   
    "
       
 Create an identity


   

       3

       >U
&
  Delete an existing DID


 

 .

     9P
L
  > Validates if particular identity was already created locally


 

 -

     8L
    =
     / Get currently installed version of edge agent


 

 !&

     1M
    -
      Signs a payload with identity


 

 +

     6I
    :
     "$, Verifies a signature tagged to the payload


     "

 ")

     "4F
    7
     &m* Signs a byte array payload with identity


 &!

 &#A

     &Lj
:
 )s- Verifies a byte array payload with identity


 )#

     )%E

 )Pp
�
     -.� To be called for registering the device identity for the first time
Expects additional device data that will be used for binding the identity with the device


 -

 -7

     -B[
�
       44� GetToken to receive required token from Scurid Platform App
Needed in order to access APIs that need authentication from the Scurid Platform App
Not used for authentication locally on the Scurid Edge Agent
Also used for refreshing the token


   4

       4

       4&1
    �
 
8C� Used for downloading one or more files
number of files to download will depend on files prepared by the admin on Scurid Platform App


 
    8

     
    8$

     
8/?
_
  ;ER invokes send data services within the Agent to send to the Scurid backend Server


  ;

      ;&

      ;1B
    �
   >v~ SendDeviceDataWithCustomFields this is used to send data to the Scurid backend Server with JSON formatted with custom fields


   >$

       >&G

   >Rs
�
 
CC� Initiates a human authentication with the device on which the Scurid Edge Agent is running
before using this make sure user has enrolled biometric data with Scurid Server; if the user has never enrolled for bio-metric data, calling this API endpoint
will not be enough. Post enrollment an administrator must grant required access rights and mapping of the user to the device


 
C

 
C%

 
C0@
�
 GG� GetDeviceUsers returns the list of users who have access to the device
this is the list of users who have enrolled with biometrics and have received access rights from the administrator


 G

 G'

 G2C
�
 LM� CreateDeviceIdentityWithAdditionalCrypto is an extended version of CreateDeviceIdentity
allows to create an identity with additional crypto artefacts


 L.

 L0[

 Lf�

 P R" left blank



 P


T V


T

 U'

 U


 U 

 U"

 U%&


Y \


Y
#
 Z" email address of the


     Z

 Z  

 Z
.
["! recorded when the user enrolled


    [

[  

[

    ^ `" left blank



    ^


b f


b
%
 c" maps to the session id


     c

 c	

 c

d

d

d  

d

e

e

e  

e


h j


h

 i

 i

 i

 i


k o


k)
4
 l"' this is the agent's / device identity


     l

 l  

 l
K
m"> array of JSON formatted data, use this for non InfluxDB data


    m


m 

m

m
4
n%"' Contains tags and fields for InfluxDB


    n


n 

n 

n#$


q s


q)

 r

 r

 r

 r


u w


u

 v" public DID


     v

 v	

 v


  y { 


      y
N
       z"A can contain any kind of on boarding information, JSON, XML etc.


       z

       z  

           z


    ~ �



    ~


         " public DID


    
     


       


     

     � �

     �
    V
      �"H short lived encrypted token returned by the server on successful login


      �

      �  

      �

     � �

     �

      �

      �

      �  

      �

     �

     �

     �  

     �
U
     �"Gidentity i.e. did:scurid:XXXXXXXXXXX ; note identity is case sensitive


     �

     �

     �

    
    � �

    
    �
    )
    
     �" true if valid, else false


    
     �

    
     �

    
     �

        � �

    �
    !
         �" data to be signed


         �

         �  

         �
V
�"H identity i.e. did:scurid:XXXXXXXXXXX ; note identity is case sensitive


        �

        �

        �

        � �

    �
    #
     �" generated signature


     �

     �  

     �

    � �

    �&
,
     �" array if bytes  to be signed


     �

     �

     �
V
�"H identity i.e. did:scurid:XXXXXXXXXXX ; NOTE identity is case sensitive


    �

    �

    �

    � �

    �&
    #
     �" generated signature


     �

     �  

     �

    � �

    �(

     �

     �

     �  

     �

    �

    �

    �

    �
V
�"Hidentity i.e. did:scurid:XXXXXXXXXXX ; NOTE: identity is case sensitive


    �

    �

    �

    � �

    �(
    =
         �"/ true if valid, else false (Defaults to False)


     �

     �

     �
    (
    � �" left blank intentionally


    �

    � �

    �

     �

     �

     �

     �

    � �

    �

     �

     �

         �

         �

        � �

        �

         �

         �

         �

         �
    .
    � �  DID to be checked if it exists


    �

     �

 �

     �

     �
J
� �< provides validation that device's DID is already generated


    �

     �

     �

     �

     �
R
� �D provides what agent version is currently installed on the hardware


    �$

     �

 �

     �  

     �
    �
        � �� sent by any client using the edge agent for generating the identity
    req expects fields of type GetDeviceIdentityReq and GetScuridEdgeAgentVersionRes


    �!
    &
     �" public identity of DID


     �

     �

     �
    
    �" time of request


    �

    �

    �
S
�"E device name provided by the Iot Software interacting with the agent


    �

    �  

    �
A
� �3 Response for registration request from the caller


    �!
I
 �"; simply a confirmation if request has been received or not


     �

     �

     �
,
    � � request structure to be sent


        �
        (
 �" device's public identity


         �

         �  

         �
        8
        �"* where ever user wants to store the files


        �

    �

    �
V
� �H true if all files are downloaded, else false - check error for details


    �
    `
 �"R True/False, confirms if the files have been downloaded, check for error if false


     �

     �

     �
c
� �U Note: ensure to acquire login token for the agent which is trying to send this data


    �
    (
         �" device's public identity


     �

     �  

     �
    '
    �" this is the json string


    �

    �  

    �

     � �

     �
e
  �"W True/False, confirms if the data has been sent successfully, check for error if false


      �

      �

      �

    !� �

    !�
    @
        ! �"2 Tags as key-value pairs (string keys and values)


    ! �

    ! �

! �
Z
!� "L Fields with various data types; values that will be signed by the identity


    !�

    !�

    !�

    "� �

    "�
        
        " ��

        " �

    " �

        " �

        " �
        

    " �

        "�

        "�


        "�

    "�

    "�

    "�


    "�

    "�

"�

    "�

"�	

        "�
        (
    #� �" left blank intentionally


        #�3

        $� �

    $�3

    $ �

    $ �

    $ �

    $ �

    $�&

    $�

    $�!

    $�$%

    $�

    $�

    $� 

    $�
    q
    %� �c ECDSAPubKey512 is used to store the 512 bit ECDSA public key
    Generate ECDSA-256 (P-512) Key Pair


    %�

    % �

    % �

    % �

    % �

    %�

    %�

    %�

    %�
    v
        &� �h AES128Key is used to store return the 128 bit AES key
    returns both the key in bytes and string format


    &�

    & �

    & �

    & �

    & �

    &�

    &�

    &�  

    &�bproto3